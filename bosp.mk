
ifdef CONFIG_CONTRIB_MVIEW_DEMO_MOST

# Targets provided by this project
.PHONY: mview-demo-most clean_mview-demo-most

# Add this to the "contrib_testing" target
contrib: mview-demo-most
clean_contrib: clean_mview-demo-most

MODULE_CONTRIB_USER_MVIEW_DEMO_MOST=contrib/user/mview-demo-most

.cmd_ok:
	$(MODULE_CONTRIB_USER_MVIEW_DEMO_MOST)/dataset/check_cmds.sh

mview-demo-most: external .cmd_ok
	@echo
	@echo "==== Building OpenMP-MView-MOST ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_USER_MVIEW_DEMO_MOST)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_USER_MVIEW_DEMO_MOST)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_MVIEW_DEMO_MOST)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_MVIEW_DEMO_MOST)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_MVIEW_DEMO_MOST)/dataset && \
		./install.sh || \
		exit 1
	@echo

clean_mview-demo-most:
	@echo
	@echo "==== Clean-up OpenMP-MView-MOST Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/ompstereomatch ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/MviewDemoMost.recipe; \
		rm -f $(BUILD_DIR)/usr/bin/mview-demo-most; \
		rm -f $(BUILD_DIR)/etc/bbque/MviewDemoMost.conf; \
		rm -f $(MODULE_CONTRIB_USER_MVIEW_DEMO_MOST)/dataset/Aloe/i-out.bmp
	@rm -rf $(MODULE_CONTRIB_USER_MVIEW_DEMO_MOST)/build
	@cd $(MODULE_CONTRIB_USER_MVIEW_DEMO_MOST)/dataset && \
		./uninstall.sh
	@echo

else # CONFIG_CONTRIB_MVIEW_DEMO_MOST

mview-demo-most:
	$(warning contrib module OpenMP-MView-MOST disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_MVIEW_DEMO_MOST

