read_script "scripts/common.scr"

#
# Generation of Operating Points (OPs)
#
db_read "databases/mosa.db" --destination="db"

set max_cpu_utiliz = 6
set max_num_ops = 100

set_function avg_disp_error(x) = avg(i, 0, $limit, get_disparity_error($x,$i))
set_function avg_cycle_ms(x)   = geomavg(i, 0, $limit, get_cycle_ms($x,$i))
set_function avg_cpu_utiliz(x) = geomavg(i, 0, $limit, get_cpu_utiliz($x,$i))

set_constraint cpu_utiliz_constraint(x) = avg_cpu_utiliz($x) < $max_cpu_utiliz

db_filter_valid "db" --violating=true

db_report "db" --only_size=true
echo "Size is " + $?


##### Argo Operating Points #####

# Optimization objectives
set_objective disparity_error(x) = avg_disp_error($x)
set_objective t_cycle_ms(x) = avg_cycle_ms($x)
set_objective cpu_utiliz(x) = avg_cpu_utiliz($x)

# Make HTML report
db_report_html "db" --objectives=true --name="report_mosa"
system("tar zcf report_mosa.tar.gz report_mosa/; rm -rf report_mosa")

# Pareto-filtering
db_filter_pareto "db" --valid=true

# Remove all design objectives
opt_remove_objective

# Obtain max and min for metric 'disparity_error'
set max_error = db_max("db", ^(x)=avg_disp_error($x))
set min_error = db_min("db", ^(x)=avg_disp_error($x))

# Obtain max and min for metric 'perf_cycles_avg_ms'
set max_cycles_avg_ms = db_max("db", ^(x)=avg_cycle_ms($x))
set min_cycles_avg_ms = db_min("db", ^(x)=avg_cycle_ms($x))

# Set OP metrics
set_objective throughput(x) = 1000.0 / avg_cycle_ms($x)
set_objective cpu_usage(x) = avg_cpu_utiliz($x)
set_objective norm_error(x) = (avg_disp_error($x) - $min_error) / ($max_error - $min_error)

# Clustering
db_compute_kmeans_clusters --clusters=$max_num_ops --iterations=100 "db"

# Export to XML format
db_export_xml "kmean_clusters" --file_name="op_list.xml" --use_objectives=true

# Write database
db_write "kmean_clusters" --file_name="databases/pareto.db"
