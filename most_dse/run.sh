#__MOST_GENERIC_WRAPPER__#                  INPUT_TEMPLATE_FILE           INPUT_FILE
#__MOST_GENERIC_WRAPPER__input_file__#      ../../MviewDemoMost.conf.in  MviewDemoMost.conf

#__MOST_GENERIC_WRAPPER__#                  METRIC_NAME        OUTPUT_FILE          TYPE         ADDITIONAL INFO
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_0  S0_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_0    S0_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_0       S0_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_1  S1_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_1    S1_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_1       S1_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_2  S2_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_2    S2_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_2       S2_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_3  S3_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_3    S3_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_3       S3_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_4  S4_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_4    S4_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_4       S4_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_5  S5_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_5    S5_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_5       S5_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_6  S6_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_6    S6_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_6       S6_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_7  S7_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_7    S7_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_7       S7_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_8  S8_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_8    S8_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_8       S8_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     disparity_error_9  S9_metrics.txt       regexp       @avg_disparity_error=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cycles_avg_ms_9    S9_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cycles_avg_ms=([\d]+(\.[\d]+)?)@
#__MOST_GENERIC_WRAPPER__output_file__#     cpu_utiliz_9       S9_metrics.txt       regexp       @[\w]+\:[\d]+\:perf\:cpu_utiliz=([\d]+(\.[\d]+)?)@



#!/bin/sh


################################################################################
# DSE Configuration
################################################################################

ROOT="${PWD}/../.."

BOSP_BASE="${BOSP_BASE:-$ROOT/../../../..}"
BBQUE_SYSROOT="${BBQUE_SYSROOT:-$BOSP_BASE/out}"
BIN_PATH="${BBQUE_SYSROOT}/usr/bin"

# Replacing the contents of the "$@" array
set -- "Aloe" "Baby1" "Bowling1" "Cloth4" "Flowerpots" "Lampshade2" "Midd2" "Monopoly" "Plastic" "Wood2"
DATASET_PATH="${BOSP_BASE}/contrib/user/mview-demo-most/dataset"
GRAYSCALE=3
NCYCLES=10

CMD="${BIN_PATH}/mview-demo-most -c MviewDemoMost.conf -p"
SIMID=`basename $PWD`


################################################################################
# Run application with RTLib metric report
################################################################################

MAX_HYPO_VALUE=@__MOST_GENERIC_WRAPPER__max_hypo_value__@
HYPO_STEP=@__MOST_GENERIC_WRAPPER__hypo_step__@
MAX_ARM_LENGTH=@__MOST_GENERIC_WRAPPER__max_arm_length__@
COLOR_THRESHOLD=@__MOST_GENERIC_WRAPPER__color_threshold__@
MATCHCOST_LIMIT=@__MOST_GENERIC_WRAPPER__matchcost_limit__@
NUM_THREADS=@__MOST_GENERIC_WRAPPER__nthreads__@

BBQUE_RTLIB_OPTS="U:DC${NCYCLES}:p0:M"

CMD="$CMD -m $MAX_HYPO_VALUE"
CMD="$CMD -s $HYPO_STEP"
CMD="$CMD -a $MAX_ARM_LENGTH"
CMD="$CMD -t $COLOR_THRESHOLD"
CMD="$CMD -l $MATCHCOST_LIMIT"
CMD="$CMD -n $NUM_THREADS"

while [ $# -gt 0 ]
do
	TEST="$1"
	TESTID=`echo "10 - $#" | bc`
	shift

	echo "# $SIMID: Test ${TESTID} : '${TEST}'"
	eval "BBQUE_RTLIB_OPTS=\"${BBQUE_RTLIB_OPTS}\" $CMD -g $GRAYSCALE -d $DATASET_PATH/$TEST 2>app_log.txt"

	# Parse application log
	sed -e '1,/\.\:\: MOST statistics for AWM/d' app_log.txt > S${TESTID}_metrics.txt

	rm -f app_log.txt
done

exit 0
