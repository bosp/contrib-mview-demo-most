# README #

This repository contains an OpenMP implementation of the Stereo-Matching
application integrated with BarbequeRTRM (http://bosp.dei.polimi.it/).

The Stereo-Matching algorithm is described in the article:
> Ke Zhang, Jiangbo Lu, and Gauthier Lafruit, "Cross-Based Local Stereo
  Matching Using Orthogonal Integral Images", IEEE Transactions on Circuits and
  Systems for Video Technology, Vol.19, pp.1073-1079.


## What is this repository for? ##

This application computes a disparity map on an input pair of stereo images.
It exposes a set of application-specifc parameters that impact on both the
execution time and the accuracy of the result. Moreover, the parameter
`nthreads` allows to control the number of OpenMP threads used in the parallel
regions.
The parameterized command-line interface enables easy integration with the MOST
framework &mdash; Multi-Objective System Tuner &mdash; developed by Politecnico
di Milano. MOST is used for automated Design Space Exploration (DSE) of the
application configuration options, to identify the optimal solutions with
respect to throughput, accuracy of the result and platform utilization.


## Instructions for Design Space Exploration ##

The `most_dse` folder contains the scripts and configuration files for Design
Space Exploration (DSE) with MOST &mdash; Multi-Objective System Tuner &mdash;
developed by Politecnico di Milano. The following instructions assume that the
working directory is set to the `most_dse` folder.

MOST should be installed in the default path:

> `/usr/local/most-pdm`

If MOST is installed in a different directory, then change the MOST variable in
the header of `Makefile`. Moreover, the OpenMP Stereo-Matching application
should already have been compiled and installed (see README in the parent
folder) at the path:

> `<BOSP_ROOT>/out/usr/bin`

The following instructions explain how to run semi-automated DSE step by step.
At this aim, these DSE scripts read a database of design points profiled on a
AMD NUMA server, used a reference platform. This database is automatically
installed in `databases/root.db` and it is imported into MOST, allowing a fast
demonstration of MOST features without real simulation of the Stereo-Matching
application. To enable real simulation on your platform, then delete (if already
installed) the default database:

> `rm -f databases/root.db`

and create an empty one:

> `touch databases/root.db`


### Dataset ###

To derive the metrics associated with different scenarios, 10 representative
datasets are used among those available in the
[Middlebury Stereo Dataset](http://vision.middlebury.edu/stereo/data/scenes2006/) (2006).
The exact names of the datasets used for DSE are listed in file `run.sh`, which
wraps the call to the application executable.


### Test configuration ###

To check that the installation is working, within the `most_dse` folder enter
the command:

> `make test`

This test stresses all the 10 reference images, running the Stereo-Matching
application in Unmanaged Mode (see Barbeque documentation) with a fixed
configuration of parameters. The values assigned to application parameters are
defined in the XML design space description:

> `design_space_test.xml`

This file lists all design parameters, explored by MOST, and the metrics
profiled by means of Barbeque _RTLib_ support.


### DSE Phases ###

This section describes the different DSE phases, and the commands to run each
step. All commands should be from a terminal, in the `most_dse` folder as the
working directory. Except for step 0, all remaining steps should be executed in
sequential order, because the output of one command is used by the next one. The
final result will be a set of Pareto-optimal configurations, with respect to
design objectives set to maximize throughput, while minimizing the disparity
error and the CPU usage.


#### Phase 0: gradient analysis ####

Enter the following command to run a gradient analysis:

> `make p0`

MOST will produce the PDF file `report-effect.pdf`, by running the script
`scripts/p0_gradient.scr`. The plots contained in this file show the impact of
design parameters on the metrics. Each plot is obtained by generating random
values of design parameters, except for one parameter, which can only assume the
minimum and maximum value. Thus, each plot shows the impact of varying the
analyzed parameter from the minimum to the maximum allowed value, independently
from the other parameters.

Horizontal or vertical arrows mean that the selected parameter impact only on
one of the two observed metrics, while oblique arrows indicate an interaction
with both metrics. The longer the line, the higher the impact. Thus, the goal of
this phase is to identify the parameters with highest impact on design metrics.
For the Stereo-Matching application, these parameters are `nthreads`,
`hypo_step` and `max_arm_length`.


#### Phase 1: full search over most important parameters ####

Enter the following command to run a full search over the most significant
parameters found at the previous step:

> `make p1`

MOST will evaluate all combinations of values for parameters `nthreads`,
`hypo_step` and `max_arm_length`. The output is a database of points, written to
file `databases/full.db`


#### Phase 2: refinement of solutions over complete design space ####

The database produced at the previous step is a subset of the complete design
space, thus it could give just an approximate Pareto-optimal solution. To refine
the solution, run the command:

> `make p2`

This command will run MOST over the entire design space, starting from the
optimal solutions found at previous step, using a Multi-Objective Simulated
Annealing (MOSA) optimizer. The result is a new database, written to file
`databases/mosa.db`


#### Phase 3: generation of Operating Points (OPs) ####

The command:

> `make p3`

will Pareto-filter the database of design points and generate a list of
Operating Points (OPs) in XML format: `op_list.xml`
This file is used as a *performance model* of the application, input to the
Application-Specific Run-Time Manager (AS-RTM) provided by the ARGO library.
The OPs provide all optimal trade-offs with respect to design objectives, in
order to enable more efficient run-time application adaptivity.

This phase also generates an HTML report of DSE results: `report_mosa.tar.gz`


#### Phase 4: generation of list of Application Working Modes (AWMs in BBQ recipe) ####

Finally, the command:

> `make p4`

takes the list of OPs and applies a K-means clustering with respect to resource
usage and throughput. The centroids of these clusters represent the Application
Working Modes (AWMs) and they are written to the application recipe
(`awm_list.xml`). This file will be used by Barbeque for optimal resource
allocation on the target platform.


## Who do I talk to? ##

Contact: edoardo [dot] paone [at] polimi [dot] it

Organization: Politecnico di Milano, Italy
