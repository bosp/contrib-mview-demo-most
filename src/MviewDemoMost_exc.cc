/**
 *       @file  MviewDemoMost_exc.cc
 *      @brief  The OpenMP-MView-MOST BarbequeRTRM application
 *
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#include "MviewDemoMost_exc.h"

#include <bbque/utils/utility.h>
#include <omp.h>

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.OmpMview"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetChUid()

MviewEXC::MviewEXC(
		std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		std::string const & datapath,
		int grayscale) :
	BbqueEXC(name, recipe, rtlib),
	exit_loop(false),
	datapath(datapath),
	grayscale(grayscale) {

	logger->Info("New MviewEXC::MviewEXC()");

	// NOTE: since RTLib 1.1 the derived class construct should be used
	// mainly to specify instantiation parameters. All resource
	// acquisition, especially threads creation, should be palced into the
	// new onSetup() method.

	logger->Info("EXC Unique IDentifier (UID): %u", GetUid());

	/* Set default parameter values */
	max_hypo_value  = 32;
	hypo_step       = 1;
	max_arm_length  = 16;
	color_threshold = 30;
	matchcost_limit = 60;
	num_threads     = 0;   // default thread pool size
}

RTLIB_ExitCode_t MviewEXC::onSetup() {
	// This is just an empty method in the current implementation of this
	// testing application. However, this is intended to collect all the
	// application specific initialization code, especially the code which
	// acquire system resources (e.g. thread creation)
	logger->Info("MviewEXC::onSetup()");

	// Read reference disparity map (gray level bitmap)
	refMap = cv::imread(datapath+"/ref_rx.bmp", CV_LOAD_IMAGE_GRAYSCALE);
	if (!refMap.data) {
		logger->Error("Failed to open reference disparity map!");
		return RTLIB_ERROR;
	}

	// Get frame size
	imgwidth  = refMap.size().width;
	imgheight = refMap.size().height;

	// Create disparity map for result
	outMap = refMap;

	// Allocate EXC buffers
	size_t num_pixels = imgwidth * imgheight;

	crosses_lx    = (cross_t *)calloc(num_pixels, sizeof(cross_t));
	crosses_rx    = (cross_t *)calloc(num_pixels, sizeof(cross_t));
	raw_matchcost = (int *)calloc(num_pixels, sizeof(int));
	matchcost     = (int *)calloc(num_pixels, sizeof(int));
	anchorreg     = (int *)calloc(num_pixels, sizeof(int));
	fmatchcost    = (int *)calloc(num_pixels, sizeof(int));
	fanchorreg    = (int *)calloc(num_pixels, sizeof(int));
	dpr_votes     = (vote_t *)calloc(num_pixels, sizeof(vote_t));
	dpr_block     = (dpr_t *)calloc(num_pixels, sizeof(dpr_t));

	return RTLIB_OK;
}

RTLIB_ExitCode_t MviewEXC::onConfigure(uint8_t awm_id) {

	logger->Warn("MviewEXC::onConfigure(): EXC [%s] => AWM [%02d]",
		exc_name.c_str(), awm_id);
#ifdef _OPENMP
	if (num_threads > 0) {
		omp_set_num_threads(num_threads);
		logger->Warn(" - OMP_NUM_THREADS ... %d", num_threads);
	}
	else {
#pragma omp parallel
		#pragma omp master
		logger->Warn(" - OMP_NUM_THREADS ... %d", omp_get_num_threads());
	}
#endif
	logger->Warn(" - MAX_HYPO_VALUE .... %d", max_hypo_value);
	logger->Warn(" - HYPO_STEP ......... %d", hypo_step);
	logger->Warn(" - MAX_ARM_LENGTH .... %d", max_arm_length);
	logger->Warn(" - COLOR_THRESHOLD ... %d", color_threshold);
	logger->Warn(" - MATCHCOST_LIMIT ... %d", matchcost_limit);

	return RTLIB_OK;
}

RTLIB_ExitCode_t MviewEXC::onRun() {
	{
		std::lock_guard<std::mutex> lock(exit_mutex);

		if (exit_loop)
			return RTLIB_EXC_WORKLOAD_NONE;
	}

	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	// Do one more cycle
	logger->Info("MviewEXC::onRun()      : EXC [%s]  @ AWM [%02d]",
		exc_name.c_str(), wmp.awm_id);

	// Read input stereo frames
	cv::Mat bitmapLx = cv::imread(datapath+"/cam_lx.bmp", CV_LOAD_IMAGE_COLOR);
	if (!bitmapLx.data) {
		logger->Error("Failed to read left frame!");
		return RTLIB_ERROR;
	}
	if ( bitmapLx.size().width != imgwidth || bitmapLx.size().height != imgheight) {
		logger->Error("Wrong size for left frame!");
		return RTLIB_ERROR;
	}
	cv::Mat bitmapRx = cv::imread(datapath+"/cam_rx.bmp", CV_LOAD_IMAGE_COLOR);
	if (!bitmapRx.data) {
		logger->Error("Failed to read right frame!");
		return RTLIB_ERROR;
	}
	if ( bitmapRx.size().width != imgwidth || bitmapRx.size().height != imgheight) {
		logger->Error("Wrong size for right frame!");
		return RTLIB_ERROR;
	}

#pragma omp parallel
{
	// Build support regions
	winBuild(crosses_lx, (const rgb_t *)bitmapLx.data);
	winBuild(crosses_rx, (const rgb_t *)bitmapRx.data);
	// Evaluate disparity hypotheses
	for (int d=0; d<=max_hypo_value; d+=hypo_step)
	{
		raw_horizontal_integral(
			(const rgb_t *)bitmapLx.data, (const rgb_t *)bitmapRx.data, d);
		horizontal_integral(d);
		vertical_integral(d);
		crossregion_integral(d);
	}
	// Refinement filter
	refinement((unsigned char *)outMap.data);
}
	// release input frames
	bitmapLx.release();
	bitmapRx.release();

	return RTLIB_OK;
}

RTLIB_ExitCode_t MviewEXC::onRelease() {

	logger->Info("MviewEXC::onRelease()");

	// Write result disparity map
	imwrite(datapath+"/i-out.bmp", outMap);

	if (crosses_lx)
		free ((void *) crosses_lx);
	if (crosses_rx)
		free ((void *) crosses_rx);
	if (raw_matchcost)
		free ((void *) raw_matchcost);
	if (matchcost)
		free ((void *) matchcost);
	if (anchorreg)
		free ((void *) anchorreg);
	if (fmatchcost)
		free ((void *) fmatchcost);
	if (fanchorreg)
		free ((void *) fanchorreg);
	if (dpr_votes)
		free ((void *) dpr_votes);
	if (dpr_block)
		free ((void *) dpr_block);

	outMap.release();
	refMap.release();

	return RTLIB_OK;
}

void MviewEXC::SetOpParams(
	int max_hypo_value,
	int hypo_step,
	int max_arm_length,
	int color_threshold,
	int matchcost_limit,
	int num_threads)
{
	this->max_hypo_value  = max_hypo_value;
	this->hypo_step       = hypo_step;
	this->max_arm_length  = max_arm_length;
	this->color_threshold = color_threshold;
	this->matchcost_limit = matchcost_limit;
	this->num_threads     = num_threads;
}



/* Application-specifc methods */

bool similar(rgb_t p, rgb_t q, int color_threshold)
{
	if (abs( (int)p.B - (int)q.B ) > color_threshold ||
	    abs( (int)p.G - (int)q.G ) > color_threshold ||
	    abs( (int)p.R - (int)q.R ) > color_threshold)
	{
		return false;
	}

	return true;
}

void MviewEXC::winBuild(cross_t * crosses, const rgb_t *bitmap)
{
	#pragma omp for
	for (int py=0; py<imgheight; py++)
	{
		for (int px=0; px<imgwidth; px++)
		{
			int c;
			const size_t offset = py*imgwidth+px;

			rgb_t p = bitmap[offset];

			// Move left
			if (px>0)
			{
				c = 1;
				while (px-c>=0 && similar(p, bitmap[py*imgwidth+px-c], color_threshold) && c<=max_arm_length)
				{
					c++;
				}

				if (c>1) c--;
			}
			else
			{
				c = 0;
			}
			// Set arm length
			crosses[offset].l = c;

			// Move right
			if (px<imgwidth-1)
			{
				c = 1;
				while (px+c<imgwidth && similar(p, bitmap[py*imgwidth+px+c], color_threshold) && c<=max_arm_length)
				{
					c++;
				}
				
				if (c>1) c--;
			}
			else
			{
				c = 0;
			}
			// Set arm length
			crosses[offset].r = c;
			
			// Move up
			if (py>0)
			{
				c = 1;
				while (py-c>=0 && similar(p, bitmap[(py-c)*imgwidth+px], color_threshold) && c<=max_arm_length)
				{
					c++;
				}

				if (c>1) c--;
			}
			else
			{
				c = 0;
			}
			// Set arm length
			crosses[offset].u = c;

			// Move down
			if (py<imgheight-1)
			{
				c = 1;
				while (py+c<imgheight && similar(p, bitmap[(py+c)*imgwidth+px], color_threshold) && c<=max_arm_length)
				{
					c++;
				}

				if (c>1) c--;
			}
			else
			{
				c = 0;
			}
			// Set arm length
			crosses[offset].d = c;
		}
	}
}

int pixel_aggr_cost(rgb_t p, rgb_t q, int matchcost_trunc)
{
	int cost = abs((int)p.B-(int)q.B) + abs((int)p.G-(int)q.G) + abs((int)p.R-(int)q.R);

	if (cost > matchcost_trunc)
		return matchcost_trunc;
	else
		return cost;
}

void MviewEXC::raw_horizontal_integral(const rgb_t *bitmapLx, const rgb_t *bitmapRx, int disp_cur)
{
	const int xlimit = imgwidth-1-disp_cur;

	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			const size_t offset = y*imgwidth+x;

			if (x>xlimit)
			{
				raw_matchcost[offset] = raw_matchcost[offset-1] + matchcost_limit;
			}
			else
			{
				int cost = pixel_aggr_cost(bitmapRx[offset], bitmapLx[offset+disp_cur], matchcost_limit);

				if (x==0)
				{
					raw_matchcost[offset] = cost;
				}
				else
				{
					raw_matchcost[offset] = raw_matchcost[offset-1] + cost;
				}
			}
		}
	}
}

void get_combined_cross(cross_t a, cross_t b, cross_t *res)
{
	if (a.d<b.d)
		res->d = a.d;
	else
		res->d = b.d;

	if (a.l<b.l)
		res->l = a.l;
	else
		res->l = b.l;

	if (a.r<b.r)
		res->r = a.r;
	else
		res->r = b.r;

	if (a.u<b.u)
		res->u = a.u;
	else
		res->u = b.u;
}

void MviewEXC::horizontal_integral(int disp_cur)
{
	const int xlimit = imgwidth-1-disp_cur;

	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			cross_t combined_cross;
			const size_t offset = y*imgwidth+x;

			if (x>xlimit)
			{
				combined_cross = crosses_rx[offset];
			}
			else
			{
				get_combined_cross(crosses_rx[offset], crosses_lx[offset+disp_cur], &combined_cross);
			}

			anchorreg[offset] = combined_cross.r + combined_cross.l + 1;

			if (x==0 || x-combined_cross.l==0)
				matchcost[offset] = raw_matchcost[offset+combined_cross.r];
			else
				matchcost[offset] = raw_matchcost[offset+combined_cross.r] - raw_matchcost[offset-combined_cross.l-1];
		}
	}
}

void MviewEXC::vertical_integral(int disp_cur)
{
	#pragma omp for
	for (int x=0; x<imgwidth; x++)
	{
		for (int y=0; y<imgheight; y++)
		{
			if (y>0)
			{
				matchcost[y*imgwidth+x] += matchcost[(y-1)*imgwidth+x];
				anchorreg[y*imgwidth+x] += anchorreg[(y-1)*imgwidth+x];
			}
		}
	}
}

void MviewEXC::crossregion_integral(int disp_cur)
{
	const int xlimit = imgwidth-1-disp_cur;

	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			cross_t combined_cross;
			const size_t offset = y*imgwidth+x;

			if (x>xlimit)
			{
				combined_cross = crosses_rx[offset];
			}
			else
			{
				get_combined_cross(crosses_rx[offset], crosses_lx[offset+disp_cur], &combined_cross);
			}

			if (y==0 || y-combined_cross.u==0)
			{
				fmatchcost[offset] = matchcost[(y+combined_cross.d)*imgwidth+x];
				fanchorreg[offset] = anchorreg[(y+combined_cross.d)*imgwidth+x];
			}
			else
			{
				fmatchcost[offset] = matchcost[(y+combined_cross.d)*imgwidth+x] - matchcost[(y-combined_cross.u-1)*imgwidth+x];
				fanchorreg[offset] = anchorreg[(y+combined_cross.d)*imgwidth+x] - anchorreg[(y-combined_cross.u)*imgwidth+x];
			}

			int costonorm = fmatchcost[offset] / (fanchorreg[offset]+1);

			if (disp_cur==0 || costonorm < dpr_votes[offset].costo)
			{
				dpr_votes[offset].costo = costonorm;
				dpr_votes[offset].disparity = (unsigned char) (disp_cur & 0xFF);
			}
		}
	}
}

void MviewEXC::refinement(unsigned char *disparity)
{
	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			size_t offset = y*imgwidth+x;

			for (int i=0; i<8; i++)
				dpr_block[offset].bitnum[i] = 0;

			for (int h = -crosses_rx[offset].l; h <= crosses_rx[offset].r; h++)
			{
				unsigned char disp = dpr_votes[offset+h].disparity;

				for (int i=0; i<8; i++)
					dpr_block[offset].bitnum[i] += (disp >> i) & 0x1;
			}

			dpr_block[offset].npixels = crosses_rx[offset].l + crosses_rx[offset].r + 1;
		}
	}

	#pragma omp for
	for (int y=0; y<imgheight; y++)
	{
		for (int x=0; x<imgwidth; x++)
		{
			size_t offset = y*imgwidth+x;

			unsigned int npixels = 0;
			unsigned int bitmap[8] = {0,0,0,0,0,0,0,0};

			for (int v = -crosses_rx[offset].u; v <= crosses_rx[offset].d; v++)
			{
				for (int i=0; i<8; i++)
					bitmap[i] += dpr_block[(y+v)*imgwidth+x].bitnum[i];

				npixels += dpr_block[(y+v)*imgwidth+x].npixels;
			}

			int disp = 0x00;
			for(int i=0; i<8; i++)
				disp |= (npixels < (bitmap[i]<<1)) ? (0x1 << i) : 0x0;

			int inty = disp * grayscale;
			if (inty > 255)
				inty = 255;
			disparity[offset] = (inty & 0xFF);
		}
	}
}

